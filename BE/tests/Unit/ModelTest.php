<?php

namespace Tests\Unit;

use App\Models\Book;
use App\Models\BorrowedBook;
use App\Models\Category;
use App\Models\Patron;
use App\Models\ReturnedBook;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class ModelTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    //Check the columns in the tables
    public function test_a_patrons_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('patrons', [
              'id', 'last_name', 'first_name', 'middle_name', 'email'
          ]), 1);
    }
    

    public function test_a_books_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('books', [
              'id', 'name', 'author', 'copies', 'category_id'
          ]), 1);
    }

    public function test_a_categories_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('categories', [
              'id', 'category'
          ]), 1);
    }

    public function test_a_borrowed_books_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('borrowed_books', [
              'id', 'patron_id', 'copies', 'book_id'
          ]), 1);
    }

    public function test_a_returned_books_table_has_the_expected_columns() {
        $this->assertTrue( 
            Schema::hasColumns('returned_books', [
              'id', 'patron_id', 'copies', 'book_id'
          ]), 1);
    }

    public function test_a_book_has_a_category() {

        //Create Category
        $category = Category::factory()->create(['category' => 'Sci-Fi']);
        //Create a book
        $book = Book::factory()->create(['category_id' => $category->id]);

        $this->assertInstanceOf(Category::class, $book->category);
        $this->assertEquals(1, $book->category->count());
    }

    public function test_a_category_belongs_to_many_books() {

        //Create Category
        $category = Category::factory()->create(['category' => 'Sci-Fi']);
        //Create a book
        $book = Book::factory()->create(['category_id' => $category->id]);
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $category->book); 
        $this->assertEquals(1, $category->book->count());
    }

    //Book - Borrowed Book
    //Book - Returned Book

    public function test_a_borrowed_books_has_many_books() {

        //Create a patron
        $patron = Patron::factory()->create();
        //Create Category
        $category = Category::factory()->create(['category' => 'Sci-Fi']);
        //Create a book
        $book = Book::factory()->create(['category_id' => $category->id]);
        //Borrow The Book
        $borrowed = BorrowedBook::make([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id
        ]);
        //Assert the instance
        $this->assertInstanceOf(Book::class, $borrowed->book);
        //Assert if has a borrowed book
        $this->assertEquals(1, $borrowed->book->count());
    }

    public function test_a_returned_books_has_many_books() {

        //Create a patron
        $patron = Patron::factory()->create();
        //Create Category
        $category = Category::factory()->create(['category' => 'Sci-Fi']);
        //Create a book
        $book = Book::factory()->create(['category_id' => $category->id]);
        //Return The Book
        $returned = ReturnedBook::make([
            'book_id' => $book->id,
            'copies' => 1,
            'patron_id' => $patron->id
        ]);
        
        //Assert the instance
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $book->returned);
    }

    public function test_a_book_belongs_to_many_borrowed_book() {
      
        //Create a patron
        $patron = Patron::factory()->create();
        //Create Category
        $category = Category::factory()->create(['category' => 'Sci-Fi']);
        //Create a book
        $book = Book::factory()->create(['category_id' => $category->id]);
        //Borrow The Book
        $borrowed = BorrowedBook::make([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id
        ]);
        
        //Assert the instance
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $book->borrowed);
    }

    public function test_a_book_belongs_to_many_returned_book() {

        //Create a patron
        $patron = Patron::factory()->create();
        //Create Category
        $category = Category::factory()->create(['category' => 'Sci-Fi']);
        //Create a book
        $book = Book::factory()->create(['category_id' => $category->id]);
        //Borrow The Book
        $returned = ReturnedBook::make([
            'book_id' => $book->id,
            'copies' => 1,
            'patron_id' => $patron->id
        ]);


        //Assert the instance
        $this->assertInstanceOf(Book::class, $returned->book);
        //Assert if has a returned book
        $this->assertEquals(1, $returned->book->count());
    }

    //Patron - Borrowed Book
    //Patron - Returned Book
    public function test_a_patron_has_many_borrowed_book() {

        //Create a patron
        $patron = Patron::factory()->create();
        //Create Category
        $category = Category::factory()->create(['category' => 'Sci-Fi']);
        //Create a book
        $book = Book::factory()->create(['category_id' => $category->id]);
        //Borrow The Book
        $borrowed = BorrowedBook::make([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id
        ]);
        //Assert the instance
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $patron->borrowed);
    }

    public function test_a_patron_has_many_returned_book() {

        //Create a patron
        $patron = Patron::factory()->create();
        //Create Category
        $category = Category::factory()->create(['category' => 'Sci-Fi']);
        //Create a book
        $book = Book::factory()->create(['category_id' => $category->id]);
        //Borrow The Book
        $returned = ReturnedBook::make([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id
        ]);
        //Assert the instance
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $patron->returned);
    }

    public function test_a_borrowed_books_belongs_to_many_patron() {

        //Create a patron
        $patron = Patron::factory()->create();
        //Create Category
        $category = Category::factory()->create(['category' => 'Sci-Fi']);
        //Create a book
        $book = Book::factory()->create(['category_id' => $category->id]);
        //Borrow The Book
        $borrowed = BorrowedBook::make([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id
        ]);

        //Assert the instance
        $this->assertInstanceOf(Patron::class, $borrowed->patron);
        //Assert if has a returned book
        $this->assertEquals(1, $borrowed->patron->count());
    }

    public function test_a_returned_books_belongs_to_many_patron() {

        //Create a patron
        $patron = Patron::factory()->create();
        //Create Category
        $category = Category::factory()->create(['category' => 'Sci-Fi']);
        //Create a book
        $book = Book::factory()->create(['category_id' => $category->id]);
        //Borrow The Book
        $returned = ReturnedBook::make([
            'patron_id' => $patron->id,
            'copies' => 1,
            'book_id' => $book->id
        ]);

        //Assert the instance
        $this->assertInstanceOf(Patron::class, $returned->patron);
        //Assert if has a returned book
        $this->assertEquals(1, $returned->patron->count());
    }

}
