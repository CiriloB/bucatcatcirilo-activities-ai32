<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBookRequest;
use App\Models\Book;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

/**
 * @group Book management
 *
 * APIs for managing Books
 */

class BookController extends Controller
{
    /**
     * Display a listing of the books.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Book::with(['category:id,category'])->get());
    }

    /**
     * Store a newly created resource in books.
     * @response
     * {
     *      "message": "Book added."
     * }
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBookRequest $request)
    {

        $book = Book::create($request->validated());
        return response()->json(['message' => 'Book added.', 'book' => $book->with(['category:id,category'])->where('id', $book->id)->firstOrFail()], 201);
    }

    /**
     * Display the specified book.
     * @response{
     *       "id": 1,
     *       "name": "E.N.D",
     *       "author": "Zeref Dragneel",
     *       "copies": 1000,
     *       "category_id": 1,
     *       "created_at": "2021-01-07T09:03:12.000000Z",
     *       "updated_at": "2021-01-07T09:03:12.000000Z",
     *       "category": {
     *          "id": 1,
     *          "category": "Science Fiction"
     *       }
     *    }
     * @response 404 {
     * 
     *  "message": "Book not found."
     * 
     * }
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            return response()->json(Book::with(['category:id,category'])->where('id', $id)->firstOrFail());
        }catch(ModelNotFoundException $e) {
            return response()->json(['message' => 'Book not found'], 404);
        }
        
    }

    /**
     * Update the specified resource in books.
     * @response {
     *
     *   "message": "Book updated."
     * 
     * }
     * 
     * @response 404 {
     * 
     *  "message": "Book not found."
     * 
     * }
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Re0sponse
     */
    public function update(StoreBookRequest $request, $id)
    {

        try {

            $book = Book::with(['category:id,category'])->where('id', $id)->firstOrFail();
            $book->update($request->validated());
            return response()->json(['message' => 'Book updated.', 'book' => $book->with(['category:id,category'])->where('id', $book->id)->firstOrFail()]);

        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Book not found'], 404);
        }
    }

    /**
     * Remove the specified resource from books.
     * @response {
     *
     *   "message": "Book deleted."
     * 
     * }
     * 
     * @response 404 {
     * 
     *  "message": "Book not found."
     * 
     * }
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $book = Book::where('id', $id)->firstOrFail();
            $book->delete();
            
            return response()->json(['message' => 'Book deleted.']);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Book not found.'], 404);

        }
    }
}
