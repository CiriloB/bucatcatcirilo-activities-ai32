<?php

namespace App\Http\Requests;

use App\Models\BorrowedBook;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class StoreReturnedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $borrowed = BorrowedBook::where([
            ['book_id', request()->get('book_id')], 
            ['patron_id', request()->get('patron_id')]
        ])->first();

        $copies = !empty($borrowed) ? $borrowed->copies : request()->get('copies');
        
        return [
            'book_id' => 'bail|required|exists:borrowed_books,book_id',
            'copies' => ['gt:0', "lte: {$copies}", 'required', 'bail'],
            'patron_id' => 'exists:borrowed_books,patron_id'
        ];
    }

     /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'book_id.exists' => 'The book doesn\'t exist in the borrowed books',
            'copies.lte' => 'The borrowed copies given exceeded the total copies of borrowed books',
            'patron_id.exists' => 'The patron doesn\'t exist in the borrowed books'
        ];
    }

    public function failedValidation(Validator $validator)
    {

        throw new HttpResponseException(response()->json(['errors' => $validator->errors()], 422));
    }

}
