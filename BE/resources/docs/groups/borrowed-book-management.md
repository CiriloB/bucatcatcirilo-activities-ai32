# Borrowed Book management

APIs for managing Borrowed Books

## Display a listing of the borrowed books.




> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/borrowedbooks" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/borrowedbooks"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 2,
        "patron_id": 1,
        "copies": 5,
        "book_id": 1,
        "created_at": "2021-01-07T10:45:46.000000Z",
        "updated_at": "2021-01-07T10:45:46.000000Z",
        "patron": {
            "id": 1,
            "last_name": "Bucatcat",
            "first_name": "Cirilo",
            "middle_name": "Espinisin",
            "email": "bucatcat1999@gmail.com",
            "created_at": "2021-01-07T08:28:27.000000Z",
            "updated_at": "2021-01-07T10:30:26.000000Z"
        },
        "book": {
            "id": 1,
            "name": "E.N.D",
            "author": "Zeref Dragneel",
            "copies": 995,
            "category_id": 1,
            "created_at": "2021-01-07T09:03:12.000000Z",
            "updated_at": "2021-01-07T10:45:46.000000Z",
            "category": {
                "id": 1,
                "category": "Science Fiction",
                "created_at": "2021-01-07T08:20:17.000000Z",
                "updated_at": "2021-01-07T08:20:17.000000Z"
            }
        }
    }
]
```
<div id="execution-results-GETapi-borrowedbooks" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-borrowedbooks"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-borrowedbooks"></code></pre>
</div>
<div id="execution-error-GETapi-borrowedbooks" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-borrowedbooks"></code></pre>
</div>
<form id="form-GETapi-borrowedbooks" data-method="GET" data-path="api/borrowedbooks" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-borrowedbooks', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-borrowedbooks" onclick="tryItOut('GETapi-borrowedbooks');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-borrowedbooks" onclick="cancelTryOut('GETapi-borrowedbooks');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-borrowedbooks" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/borrowedbooks</code></b>
</p>
</form>


## Store a newly created resource in borrowed books.




> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/borrowedbooks" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"book_id":"animi","copies":"totam","patron_id":{}}'

```

```javascript
const url = new URL(
    "http://localhost:8000/api/borrowedbooks"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "book_id": "animi",
    "copies": "totam",
    "patron_id": {}
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (200):

```json
{
    "message": "Book borrowed!"
}
```
<div id="execution-results-POSTapi-borrowedbooks" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-borrowedbooks"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-borrowedbooks"></code></pre>
</div>
<div id="execution-error-POSTapi-borrowedbooks" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-borrowedbooks"></code></pre>
</div>
<form id="form-POSTapi-borrowedbooks" data-method="POST" data-path="api/borrowedbooks" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-borrowedbooks', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-borrowedbooks" onclick="tryItOut('POSTapi-borrowedbooks');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-borrowedbooks" onclick="cancelTryOut('POSTapi-borrowedbooks');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-borrowedbooks" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/borrowedbooks</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>book_id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="book_id" data-endpoint="POSTapi-borrowedbooks" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>copies</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="copies" data-endpoint="POSTapi-borrowedbooks" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>patron_id</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="patron_id" data-endpoint="POSTapi-borrowedbooks" data-component="body"  hidden>
<br>
</p>

</form>



