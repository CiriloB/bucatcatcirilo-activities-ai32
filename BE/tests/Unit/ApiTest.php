<?php

namespace Tests\Unit;

use App\Models\Book;
use App\Models\BorrowedBook;
use App\Models\Category;
use App\Models\Patron;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ApiTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected $patron, $category, $book, $borrowed, $returned;

    public function setUp():void {

        parent::setUp();

        $this->patron = Patron::factory()->create();

        $this->category = Category::factory()->create();
        
        $this->book = Book::factory()->create(['category_id' => $this->category->id]);
    }

    //Patrons
    public function test_can_store_patron() {

        $formData = [
            'last_name' => $this->faker->lastName(),
            'first_name' => $this->faker->firstName(),
            'middle_name' => $this->faker->lastName(),
            'email' => $this->faker->unique()->safeEmail
        ];

        $this->json('POST', route('patrons.store'), $formData)
            ->assertStatus(201)
            ->assertJson(["message" => "Patron added."]);

    }

    public function test_can_get_patron() {

        $this->get(route('patrons.index'))
            ->assertJson([$this->patron->toArray()])
            ->assertStatus(200);
        
    }

    public function test_can_show_patron() {

        $this->get(route('patrons.show', $this->patron->id))->assertStatus(200);
        
    }

    public function test_can_update_patron() {

        $newData = [
            'last_name' => $this->faker->lastName(),
            'first_name' => $this->faker->firstName(),
            'middle_name' => $this->faker->lastName(),
            'email' => $this->faker->unique()->safeEmail
        ];

        $this->json('PUT', route('patrons.update', $this->patron->id), $newData)
            ->assertStatus(200)
            ->assertJson(["message" => "Patron updated."]);

    }

    public function test_can_delete_patron() {

        $this->delete(route('patrons.destroy', $this->patron->id))
            ->assertStatus(200)
            ->assertJson(["message" => "Patron deleted."]);
        
    }

    //Book
    public function test_can_store_book() {

        $formData = [
            'name' => 'Java Programming II',
            'author' => $this->faker->name(),
            'copies' => 100,
            'category_id' => $this->category->id
        ];

        $this->json('POST', route('books.store'), $formData)
            ->assertStatus(201)
            ->assertJson(["message" => "Book added."]);

    }

    public function test_can_get_book() {

        $this->get(route('books.index'))
            ->assertJson([$this->book->toArray()])
            ->assertStatus(200);
        
    }

    public function test_can_show_book() {

        $this->get(route('books.show', $this->book->id))->assertStatus(200);
        
    }

    public function test_can_update_book() {

        $newData = [
            'name' => 'Java Programming II',
            'author' => $this->faker->name(),
            'copies' => 3,
            'category_id' => $this->category->id
        ];

        $this->json('PUT', route('books.update', $this->book->id), $newData)
            ->assertStatus(200)
            ->assertJson(["message" => "Book updated."]);

    }

    public function test_can_delete_book() {

        $this->delete(route('books.destroy', $this->book->id))
            ->assertStatus(200)
            ->assertJson(['message' => 'Book deleted.']);
        
    }

    //Category
    public function test_can_get_category() {

        $this->get(route('categories.index'))
            ->assertJson([$this->category->toArray()])
            ->assertStatus(200);
        
    }

    //Borrowed Book
    public function test_can_borrow_a_book() {

        $this->borrowed = [
            'patron_id' => $this->patron->id,
            'copies' => 1,
            'book_id' => $this->book->id
        ];

        $this->returned = $this->borrowed;

        $this->json('POST', route('borrowed_books.store'), $this->borrowed)
            ->assertStatus(201)
            ->assertJson(["message" => "Book borrowed."]);

    }

    public function test_can_get_borrowed_book() {
        $this->get(route('borrowed_books.index'))
            ->assertStatus(200);
    }

    //Returned Book
    public function test_can_return_a_book() {

        $this->json('POST', route('returned_books.store'), $this->returned->toArray())
            ->assertStatus(201)
            ->assertJson(["message" => "Book retuned."]);

    }
    
    public function test_can_get_returned_book() {
        $this->get(route('returned_books.index'))
            ->assertStatus(200);
    }
}
