$(window).scroll(function () {
  var header = $("header"),
    scroll = $(window).scrollTop();
  if (scroll >= 100) header.addClass("sticky");
  else header.removeClass("sticky");
});
