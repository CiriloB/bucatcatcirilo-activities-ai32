// The data for our dataset
const data = {
  labels: ["Romance", "Fiction", "Adult", "History", "Suspense"],
  datasets: [
    {
      data: [4, 2, 0.1, 5, 3],
      backgroundColor: ["#FF6384", "#E36D2A", "#FFD36B", "#48BFBF", "#36A2EB"],
    },
  ],
};

const options = {
  options: {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    responsive: true,
    maintainAspectRatio: false
  },
}

module.exports = {data, options}