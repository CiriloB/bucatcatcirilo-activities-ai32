<?php

namespace App\Http\Requests;

use App\Models\Book;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class StoreBorrowedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $book = Book::find(request()->get('book_id'));

        $copies = !empty($book) ? $book->copies : request()->get('copies');

        return [
            'book_id' => 'bail|required|exists:books,id',
            'copies' => ['required',"lte: {$copies}", 'bail', 'gt:0'],
            'patron_id' => 'exists:patrons,id',
        ];
    
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'book_id.exists' => 'The book doesn\'t exist in the database',
            'copies.lte' => 'The borrowed copies given exceeded the total copies of book',
            'patron_id.exists' => 'The patron doesn\'t exist in the database'
        ];
    }

    public function failedValidation(Validator $validator){

        throw new HttpResponseException(response()->json([ 'errors' => $validator->errors()], 422));
    }

}
