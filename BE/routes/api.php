<?php

use App\Http\Controllers\Api\BookController as ApiBookController;
use App\Http\Controllers\Api\CategoryController as ApiCategoryController;
use App\Http\Controllers\Api\PatronController as ApiPatronController;
use App\Http\Controllers\Api\BorrowedBookController as ApiBorrowedBookController;
use App\Http\Controllers\Api\ReturnedBookController as ApiReturnedBookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResources([
    'patrons' => ApiPatronController::class,
    'books' => ApiBookController::class,
]);

Route::apiResource('categories', ApiCategoryController::class)->only('index');
Route::apiResource('borrowedbooks', ApiBorrowedBookController::class)->only(['index', 'store']);
Route::apiResource('returnedbooks', ApiReturnedBookController::class)->only(['index', 'store']);
