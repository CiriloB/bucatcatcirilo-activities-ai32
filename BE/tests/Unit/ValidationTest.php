<?php

namespace Tests\Unit;

use Tests\TestCase;

class ValidationTest extends TestCase
{

    public function test_book_can_validate_post_request()
    {

        $this->json('POST', 'api/books')->assertStatus(422);
    }

    public function test_book_can_validate_put_request()
    {

        $this->json('PUT', 'api/books/1')->assertStatus(422);
    }

    public function test_patron_can_validate_post_request()
    {

        $this->json('POST', 'api/patrons')->assertStatus(422);
    }

    public function test_patron_can_validate_put_request()
    {

        $this->json('PUT', 'api/patrons/1')->assertStatus(422);
    }

    public function test_borrowed_book_can_validate_post_request()
    {

        $this->json('POST', 'api/borrowedbooks')->assertStatus(422);

    }

    public function test_returned_book_can_validate_post_request()
    {

        $this->json('POST', 'api/returnedbooks')->assertStatus(422);
    }
}
