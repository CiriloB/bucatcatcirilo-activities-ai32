<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;

class StoreBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'bail|required|max:255',
            'author' => 'bail|required|max:255',
            'copies' => 'bail|required|integer|gt:0',
            'category_id' => 'bail|required|exists:categories,id'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Name is required.',
            'name.max' => 'Name must maximum of 255 characters.',
            'author.required' => 'Author is required.',
            'author.max' => 'Author must maximum of 255 characters.',
            'category.required' => 'Book belongs to a category.',
            'copies.integer' => 'Copies must an integer.',
            'copies.gt' => 'Copies must greater than 0.',
        ];
    }

    public function failedValidation(Validator $validator){

        throw new HttpResponseException(response()->json(['errors' => $validator->errors()], 422));
    }

}
