<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorePatronRequest;
use App\Models\Patron;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;

/**
 * @group Patron management
 *
 * APIs for managing Patron
 */
class PatronController extends Controller
{
    /**
     * Display a listing of the patrons.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Patron::all());
    }

    /**
     * Store a newly created resource in patrons.
     * @response {
     *      "message": "Patron added."
     * }
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePatronRequest $request)
    {
        //
        $patron = Patron::create($request->validated());
        return response()->json(['message' => 'Patron added.', 'patron' => $patron], 201);
    }

    /**
     * Display the specified patrons.
     * 
     * 
     * @response {
     *      "id": 1,
     *      "last_name": "Bucatcat",
     *      "first_name": "Cirilo",
     *      "middle_name": "Espinisin",
     *      "email": "bucatcat1999@gmail.com",
     *      "created_at": "2021-01-07T08:28:27.000000Z",
     *      "updated_at": "2021-01-07T08:28:27.000000Z"
     * }
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        try {
            return response()->json(Patron::where('id', $id)->firstOrFail());
        }catch(ModelNotFoundException $e) {
            return response()->json(['message' => 'Patron not found'], 404);
        }
    }

    /**
     * Update the specified resource in patrons.
     * @response {
     *
     *   "message": "Patron updated."
     * 
     * }
     * 
     * @response 404 {
     * 
     *  "message": "Patron not found."
     * 
     * }
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePatronRequest $request, $id)
    {
        //
        $patron = Patron::where('id', $id)->firstOrFail();
        $patron->update($request->validated());
        return response()->json(['message' => 'Patron updated.', 'patron' => $patron]);
    }

    /**
     * Remove the specified resource from patrons.
     * @response {
     *
     *   "message": "Patron deleted."
     * 
     * }
     * 
     * @response 404 {
     * 
     *  "message": "Patron not found."
     * 
     * }
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {

            $patron = Patron::where('id', $id)->firstOrFail();
            $patron->delete();
            
            return response()->json(['message' => 'Patron deleted.']);
        } catch (ModelNotFoundException $e) {
            return response()->json(['message' => 'Patron not found.'], 404);

        }
    }
}
