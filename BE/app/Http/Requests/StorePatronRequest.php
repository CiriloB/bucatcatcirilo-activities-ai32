<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest;

class StorePatronRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'last_name' => 'bail|required|min:3|max:20',
            'first_name' => 'bail|required|min:3|max:20',
            'middle_name' => 'bail|required|min:3|max:20',
            'email' => 'bail|required|email|unique:patrons'
        ];
    }

     /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'first_name.required' => 'First name is required.',
            'first_name.min' => 'First name must minimum of 3 characters.',
            'first_name.max' => 'First name must maximum of 50 characters.',
            'last_name.required' => 'Last name is required.',
            'last_name.min' => 'Last name must minimum of 3 characters.',
            'last_name.max' => 'Last name must maximum of 50 characters.',
            'middle_name.required' => 'Middle name is required.',
            'middle_name.min' => 'Middle name must minimum of 3 characters.',
            'middle_name.max' => 'Middle name must maximum of 50 characters.',
            'email.required' => 'Email address is required.',
            'email.email' => 'Email address must be valid.',
            'email.unique' => 'Email address already exist.'
        ];
    }

    public function failedValidation(Validator $validator){

        throw new HttpResponseException(response()->json(['errors' => $validator->errors()], 422));
    }

}
