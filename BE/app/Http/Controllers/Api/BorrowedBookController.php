<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreBorrowedBookRequest;
use App\Models\BorrowedBook;

/**
 * @group Borrowed Book management
 *
 * APIs for managing Borrowed Books
 */

class BorrowedBookController extends Controller
{
    /**
     * Display a listing of the borrowed books.
     * @reponse [
     *       {
     *           "id": 1,
     *           "patron_id": 1,
     *           "copies": 5,
     *           "book_id": 1,
     *           "created_at": "2021-01-07T10:34:46.000000Z",
     *           "updated_at": "2021-01-07T10:34:46.000000Z",
     *           "patron": {
     *               "id": 1,
     *               "last_name": "Bucatcat",
     *               "first_name": "Cirilo",
     *               "middle_name": "Espinisin",
     *               "email": "bucatcat1999@gmail.com",
     *               "created_at": "2021-01-07T08:28:27.000000Z",
     *               "updated_at": "2021-01-07T10:30:26.000000Z"
     *           },
     *           "book": {
     *               "id": 1,
     *               "name": "E.N.D",
     *               "author": "Zeref Dragneel",
     *               "copies": 995,
     *               "category_id": 1,
     *               "created_at": "2021-01-07T09:03:12.000000Z",
     *               "updated_at": "2021-01-07T10:34:46.000000Z",
     *               "category": {
     *                   "id": 1,
     *                   "category": "Science Fiction",
     *                   "created_at": "2021-01-07T08:20:17.000000Z",
     *                   "updated_at": "2021-01-07T08:20:17.000000Z"
     *               }
     *          }
     *      }
     *  ]
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(BorrowedBook::with(['patron', 'book', 'book.category'])->get());
    }

    /**
     * Store a newly created resource in borrowed books.
     * @response {
     *      "message": "Book borrowed!"
     * }
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBorrowedBookRequest $request)
    {

          $create_borrowed_book = BorrowedBook::create($request->only(['book_id', 'copies', 'patron_id']));
          $borrowed_book = BorrowedBook::with(['book'])->find($create_borrowed_book->id);

          $borrowed_book->book->update(['copies' => $borrowed_book->book->copies - $request->copies]);

          return response()->json(['message' => 'Book borrowed', 'borrowed' => $borrowed_book]);
    }
}
