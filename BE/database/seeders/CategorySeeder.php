<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            'Science Fiction',
            'Computer Science',
            'Horror',
            'Romance',
            'Fiction',
            'Fantasy'
        ];

        foreach($categories as $catergory) {
            Category::create(['category' => $catergory]);
        }
    }
}
