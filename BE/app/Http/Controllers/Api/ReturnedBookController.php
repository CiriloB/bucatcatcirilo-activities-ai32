<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreReturnedBookRequest;
use App\Models\BorrowedBook;
use App\Models\ReturnedBook;

/**
 * @group Returned Book management
 *
 * APIs for managing Returned Books
 */

class ReturnedBookController extends Controller
{
    /**
     * Display a listing of the retuned books.
     *
     * @response  [
     *       {
     *           "id": 1,
     *           "book_id": 1,
     *           "copies": 5,
     *           "patron_id": 1,
     *           "created_at": "2021-01-07T10:38:05.000000Z",
     *           "updated_at": "2021-01-07T10:38:05.000000Z",
     *           "book": {
     *               "id": 1,
     *               "name": "E.N.D",
     *               "author": "Zeref Dragneel",
     *               "copies": 1000,
     *               "category_id": 1,
     *               "created_at": "2021-01-07T09:03:12.000000Z",
     *               "updated_at": "2021-01-07T10:38:05.000000Z",
     *               "category": {
     *                   "id": 1,
     *                   "category": "Science Fiction",
     *                   "created_at": "2021-01-07T08:20:17.000000Z",
     *                   "updated_at": "2021-01-07T08:20:17.000000Z"
     *               }
     *           },
     *           "patron": {
     *               "id": 1,
     *               "last_name": "Bucatcat",
     *               "first_name": "Cirilo",
     *               "middle_name": "Espinisin",
     *               "email": "bucatcat1999@gmail.com",
     *               "created_at": "2021-01-07T08:28:27.000000Z",
     *               "updated_at": "2021-01-07T10:30:26.000000Z"
     *           }
     *       }
     *   ]
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(ReturnedBook::with(['book', 'patron', 'book.category'])->get());
    }

    /**
     * Store a newly created resource in retuned books.
     * @response {
     *      "message": "Book returned!"
     * }
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreReturnedBookRequest $request)
    {
        $borrowed_book = BorrowedBook::where([['book_id', $request->book_id], ['patron_id', $request->patron_id]])->firstOrFail();

        if(empty($borrowed_book)) {
            
            return response()->json(['message' => 'Book not found.'], 404);

        } else {

            if($borrowed_book->copies == $request->copies) {

                $borrowed_book->delete();
    
            }else {
    
                $borrowed_book->update(['copies' => $borrowed_book->copies - $request->copies]);
    
            }
    
        }

        $created_returned_book = ReturnedBook::create($request->all());
        
        $returned_book = ReturnedBook::with(['book'])->find($created_returned_book->id);
        $returned_book->book->update(['copies' => $returned_book->book->copies + $request->copies]);

        return response()->json(['message' => 'Book returned!', 'returned' => $returned_book]);
    }

}
