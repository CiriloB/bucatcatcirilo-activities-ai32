// The data for our dataset
const data = {
  labels: ["January", "February", "March", "April", "May", "June", "July"],
  datasets: [
    {
      label: "Borrowed",
      backgroundColor: "rgba(65, 184, 131, 0.2)",
      borderColor: "rgba(65, 184, 131, 1)",
      borderWidth: 1,
      data: [10, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
    },
    {
      label: "Returned",
      backgroundColor: "rgba(20, 26, 32, 0.2)",
      borderColor: "rgba(20, 26, 32, 1)",
      borderWidth: 1,
      data: [100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0],
    },
  ],
};

const options = {
  options: {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
    responsive: true,
    maintainAspectRatio: false
  },
}

module.exports = {data, options}
