<?php

use Illuminate\Support\Facades\Route;
use App\Models\Book;
use App\Models\Patron;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/patrons', function () {
    $patrons = Patron::factory()->count(10)->create();
    return $patrons;
});

Route::get('/books', function () {
    $books = Book::factory()->count(10)->create();
    return $books;
});
