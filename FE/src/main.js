import Vue from "vue";
import App from "./App";
import router from "./router";
import store from "./store";
import toastr from "@/assets/js/toastr";
import { BootstrapVue } from "bootstrap-vue";


import "bootstrap/dist/css/bootstrap.css";
import "bootstrap-vue/dist/bootstrap-vue.css";
import "toastr/build/toastr.css";
import "@/assets/css/style.css";

Vue.component('pagination', require('laravel-vue-pagination'));
Vue.use(BootstrapVue);
Vue.use(toastr);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
