# Returned Book management

APIs for managing Returned Books

## Display a listing of the retuned books.




> Example request:

```bash
curl -X GET \
    -G "http://localhost:8000/api/returnedbooks" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json"
```

```javascript
const url = new URL(
    "http://localhost:8000/api/returnedbooks"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};


fetch(url, {
    method: "GET",
    headers,
}).then(response => response.json());
```


> Example response (200):

```json
[
    {
        "id": 1,
        "book_id": 1,
        "copies": 5,
        "patron_id": 1,
        "created_at": "2021-01-07T10:38:05.000000Z",
        "updated_at": "2021-01-07T10:38:05.000000Z",
        "book": {
            "id": 1,
            "name": "E.N.D",
            "author": "Zeref Dragneel",
            "copies": 1000,
            "category_id": 1,
            "created_at": "2021-01-07T09:03:12.000000Z",
            "updated_at": "2021-01-07T10:38:05.000000Z",
            "category": {
                "id": 1,
                "category": "Science Fiction",
                "created_at": "2021-01-07T08:20:17.000000Z",
                "updated_at": "2021-01-07T08:20:17.000000Z"
            }
        },
        "patron": {
            "id": 1,
            "last_name": "Bucatcat",
            "first_name": "Cirilo",
            "middle_name": "Espinisin",
            "email": "bucatcat1999@gmail.com",
            "created_at": "2021-01-07T08:28:27.000000Z",
            "updated_at": "2021-01-07T10:30:26.000000Z"
        }
    }
]
```
<div id="execution-results-GETapi-returnedbooks" hidden>
    <blockquote>Received response<span id="execution-response-status-GETapi-returnedbooks"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-GETapi-returnedbooks"></code></pre>
</div>
<div id="execution-error-GETapi-returnedbooks" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-GETapi-returnedbooks"></code></pre>
</div>
<form id="form-GETapi-returnedbooks" data-method="GET" data-path="api/returnedbooks" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('GETapi-returnedbooks', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-GETapi-returnedbooks" onclick="tryItOut('GETapi-returnedbooks');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-GETapi-returnedbooks" onclick="cancelTryOut('GETapi-returnedbooks');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-GETapi-returnedbooks" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-green">GET</small>
 <b><code>api/returnedbooks</code></b>
</p>
</form>


## Store a newly created resource in retuned books.




> Example request:

```bash
curl -X POST \
    "http://localhost:8000/api/returnedbooks" \
    -H "Content-Type: application/json" \
    -H "Accept: application/json" \
    -d '{"book_id":"molestiae","copies":"provident","patron_id":{}}'

```

```javascript
const url = new URL(
    "http://localhost:8000/api/returnedbooks"
);

let headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
};

let body = {
    "book_id": "molestiae",
    "copies": "provident",
    "patron_id": {}
}

fetch(url, {
    method: "POST",
    headers,
    body: JSON.stringify(body),
}).then(response => response.json());
```


> Example response (200):

```json
{
    "message": "Book returned!"
}
```
<div id="execution-results-POSTapi-returnedbooks" hidden>
    <blockquote>Received response<span id="execution-response-status-POSTapi-returnedbooks"></span>:</blockquote>
    <pre class="json"><code id="execution-response-content-POSTapi-returnedbooks"></code></pre>
</div>
<div id="execution-error-POSTapi-returnedbooks" hidden>
    <blockquote>Request failed with error:</blockquote>
    <pre><code id="execution-error-message-POSTapi-returnedbooks"></code></pre>
</div>
<form id="form-POSTapi-returnedbooks" data-method="POST" data-path="api/returnedbooks" data-authed="0" data-hasfiles="0" data-headers='{"Content-Type":"application\/json","Accept":"application\/json"}' onsubmit="event.preventDefault(); executeTryOut('POSTapi-returnedbooks', this);">
<h3>
    Request&nbsp;&nbsp;&nbsp;
        <button type="button" style="background-color: #8fbcd4; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-tryout-POSTapi-returnedbooks" onclick="tryItOut('POSTapi-returnedbooks');">Try it out ⚡</button>
    <button type="button" style="background-color: #c97a7e; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-canceltryout-POSTapi-returnedbooks" onclick="cancelTryOut('POSTapi-returnedbooks');" hidden>Cancel</button>&nbsp;&nbsp;
    <button type="submit" style="background-color: #6ac174; padding: 5px 10px; border-radius: 5px; border-width: thin;" id="btn-executetryout-POSTapi-returnedbooks" hidden>Send Request 💥</button>
    </h3>
<p>
<small class="badge badge-black">POST</small>
 <b><code>api/returnedbooks</code></b>
</p>
<h4 class="fancy-heading-panel"><b>Body Parameters</b></h4>
<p>
<b><code>book_id</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="book_id" data-endpoint="POSTapi-returnedbooks" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>copies</code></b>&nbsp;&nbsp;<small>string</small>  &nbsp;
<input type="text" name="copies" data-endpoint="POSTapi-returnedbooks" data-component="body" required  hidden>
<br>
</p>
<p>
<b><code>patron_id</code></b>&nbsp;&nbsp;<small>string</small>     <i>optional</i> &nbsp;
<input type="text" name="patron_id" data-endpoint="POSTapi-returnedbooks" data-component="body"  hidden>
<br>
</p>

</form>



