import Vue from "vue";
import VueRouter from "vue-router";
import Dashboard from "@/components/pages/dashboard/Dashboard";
import Patron from "@/components/pages/patron/Patron";
import Book from "@/components/pages/book/Book";
import Settings from "@/components/pages/settings/Settings";


Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Dashboard",
    component: Dashboard,
  },
  {
    path: "/patron",
    name: "Patron",
    component: Patron,
  },
  {
    path: "/book",
    name: "Book",
    component: Book,
  },
  {
    path: "/settings",
    name: "Settings",
    component: Settings,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
