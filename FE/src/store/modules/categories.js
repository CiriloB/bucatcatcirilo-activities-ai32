import axios from "@/assets/js/config/axiosConfig";

export default {
  namespaced: true,

  state: {
    categories: [],
  },
  getters: {
    getCategories: (state) => state.categories,
  },
  mutations: {
    setCategories: (state, categories) => (state.categories = categories),
  },
  actions: {
    async fetchCategories({ commit }) {
      await axios
        .get("/categories")
        .then((res) => {
          commit("setCategories", res.data);
        })
        .catch((res) => {
          console.log(res);
        });
    },
  },
};
