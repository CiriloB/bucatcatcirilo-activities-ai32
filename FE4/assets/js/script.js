// Modal
function modalAddBook() {
  alert("qweqwewq");
}

var ctx1 = document.getElementById("myChartBar").getContext("2d");

var myChartBar = new Chart(ctx1, {
  // The type of chart we want to create
  type: "bar",

  // The data for our dataset
  data: {
    labels: ["January", "February", "March", "April", "May", "June", "July"],
    datasets: [
      {
        barPercentage: 0.5,
        barThickness: 6,
        maxBarThickness: 10,
        minBarLength: 2,
        label: "Borrowed",
        backgroundColor: "rgba(65, 184, 131, 0.2)",
        borderColor: "rgba(65, 184, 131, 1)",
        borderWidth: 1,
        data: [10, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100],
      },
      {
        label: "Returned",
        backgroundColor: "rgba(20, 26, 32, 0.2)",
        borderColor: "rgba(20, 26, 32, 1)",
        borderWidth: 1,
        data: [100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0],
      },
    ],
  },
  options: {
    responsive: true,
    legend: {
      position: "top",
    }
  },
});

var ctx2 = document.getElementById("myChartPie").getContext("2d");

var myChartPie = new Chart(ctx2, {
  // The type of chart we want to create
  type: "doughnut",

  // The data for our dataset
  data: {
    labels: ["Romance", "Fiction", "Adult", "History", "Suspense"],
    datasets: [
      {
        data: [4, 2, 0.1, 5, 3],
        backgroundColor: [
          "#FF6384",
          "#E36D2A",
          "#FFD36B",
          "#48BFBF",
          "#36A2EB",
        ],
      },
    ],
  },

  // Configuration options go here
  options: {
    responsive: true,
    legend: {
      position: "top",
    },
    animation: {
      animateScale: true,
      animateRotate: true,
    },
  },
});
