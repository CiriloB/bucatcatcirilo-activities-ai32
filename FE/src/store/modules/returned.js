import axios from "@/assets/js/config/axiosConfig";

export default {
  namespaced: true,
  state: {},
  getters: {},
  mutations: {},
  actions: {
    async returnedBook({ commit }, returned) {
      const response = await axios
        .post("/returnedbooks", returned)
        .then((res) => {
          return res;
        })
        .catch((err) => {
          return err.response;
        });
      return response;
    },
  },
};
